---
author: Gautier Steeve
title: Ensemble de nombres
---

## I. Ensemble de nombres

!!! note "Notation"
    
    Les accolades **{ ...}** servent à écrire les ensembles constitués d’une liste de nombres. On sépare les nombres de la liste avec des points virgules.

!!! abstract "Définition"

    <span style="color: #FF0000">L’ensemble {0 ; 1 ; 2 ; 3 ; …} est l’ensemble des nombres entiers naturels. On le note _N_.</span>
    
    <span style="color: #FF0000">L’ensemble {… ; - 3 ; - 2 ; - 1 ; 0 ; 1 ; 2 ; 3 ; …} est l’ensemble des nombres entiers relatifs. On le note _Z_.</span>
    
!!! warning "Remarque"

    On utilisera le symbole \, qui se lit « privé de ».

    Par exemple, l’ensemble des entiers naturels privé du nombre 10 s’écrit _N_\{10}.

!!! note "Cas particulier"

    _N_\{0} s’écrit _N_^* de même _Z_^* pour _Z_\{0}.

!!! abstract "Définition"

    <span style="color: #FF0000">L’ensemble des nombres décimaux se note _ⅅ_.</span>
    
    <span style="color: #FF0000">Caractérisation d’un nombre décimal :</span>

    <span style="color: #FF0000">Un nombre décimal est un nombre pouvant s’écrire sous la forme  _a_/(10)^_n_  avec _a_ ∈ _Z_ et _n_ ∈ _N_.</span>

!!! example "Exemples :"

    <span style="color: #0000FF">**1.** -17,899 = - 17899/(10)^3 donc -17,899 ∈ _ⅅ_.</span> 
    
    <span style="color: #0000FF">**2.** 30/8 = 3,75 = 375/(10)^2   donc  30/8 ∈ _ⅅ_.</span> 
    
    <span style="color: #0000FF">**3.** 1/3 ne peut pas s’écrire sous la forme  _a_/(10)^_n_   donc ce n’est pas un nombre décimal.  13 n'appartient pas à _ⅅ_.</span>

!!! abstract "Définition"

    <span style="color: #FF0000">L’ensemble des nombres rationnels se note _Q_.</span>
    
    <span style="color: #FF0000">Caractérisation d’un nombre rationnel :</span>

    <span style="color: #FF0000">Un nombre rationnel est un nombre pouvant s’écrire sous la forme _p_/_q_  avec _p_ ∈ _Z_ et _q_ ∈ _N_.</span>

!!! note "Vocabulaire"

    Lorsqu’un nombre **n’appartient pas** à _Q_, on dit qu’il est **irrationnel**.

!!! example "Exemples :"

    <span style="color: #0000FF">**1.** 1/3 ∈ _Q_.</span> 
    
    <span style="color: #0000FF">**2.** 5 = 5/1  donc 5 ∈ _Q_, on a aussi : 5 ∈ _N_; 5 ∈ _Z_ et 5 ∈ _ⅅ_.</span> 
    
    <span style="color: #0000FF">**3.** La valeur exacte de la diagonale d’un carré de côté 1 est √2.</span>
    
    <span style="color: #0000FF">Le nombre √2 ne peut pas s’écrire sous forme de fraction, ce n’est pas un rationnel.</span>

!!! abstract "Définition"

    <span style="color: #FF0000">L’ensemble des abscisses d’une droite graduée s’appelle l’ensemble des nombres réels se note _R_ (On parle d’ailleurs souvent de la « droite des réels »).</span>
    
!!! warning "Remarque"

    Lorsque l’on réunit l’ensemble des nombres rationnels et des nombres irrationnels, on obtient _R_.

!!! example "Exemples :"

    <span style="color: #0000FF">√5; -16; -5/2; 2Pi/3 … sont des réels.</span>

!!! abstract "Propriété"

    <span style="color: #FF0000">Ces ensembles sont inclus les uns dans les autres.</span> 
    
    <span style="color: #FF0000">On note : _N_ ⊂ _Z_ ⊂ _D_ ⊂ _Q_ ⊂ _R_.</span>
    
    <span style="color: #FF0000">Le symbole ⊂ se lit « inclus dans ».</span>

## II. Encadrer et arrondir un nombre réel

!!! abstract "Définition"

    <span style="color: #FF0000">Encadrer un nombre _x_, c’est trouver deux nombres _a_ et _b_ tels que :  _a_ < _x_ < _b_ (Ou avec :  <= ).</span> 
    
    <span style="color: #FF0000">Arrondir un nombre, c’est lui trouver la valeur la plus proche avec une précision donnée.</span>
    
???+ question "Exercices :"

    <span style="color: #0000FF">**1.** Donner un encadrement à 10^(-3) près de 2/3.</span> 
    
    ??? success "Solution"

        2/3 ≈ 0,6667  donc à 10^(-3) près :   0,666 < 2/3 < 0,667

        L’amplitude de cet encadrement est 10^(-3).

    <span style="color: #0000FF">**2.** Donner un encadrement de √39.</span> 
    
    ??? success "Solution"

        39 est compris entre les deux « carrés parfaits » 36 et 49.

        Donc √36 < √39 < √49  c'est-à-dire :   6 < √39 < 7.

        Cet encadrement a pour amplitude 1. C’est un « encadrement à l’unité ».

    <span style="color: #0000FF">**3.** Donner l’arrondi au centième de √39.</span>

    ??? success "Solution"

        A l’aide de la calculatrice, on a √39 ≈ 6,2449 donc 6,24 < √39 < 6,25.
        
        Mais le chiffre des millièmes étant un 4, on est plus proche de 6,24 que de 6,25.
        
        On écrira donc : √39 ≈ 6,24.


## III. Identifier, représenter et utiliser un intervalle


### 1. Notation et schématisation

### 2. Symboles d'appartenance et encadrements

### 3. Union d'intervalles

### 4. Intersection d'intervalles
