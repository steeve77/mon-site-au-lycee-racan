---
author: Gautier Steeve
title: Proportions et pourcentages
---

## I. Proportion d’une sous-population dans une population

!!! abstract "Définition"

    <span style="color: #FF0000">La proportion (ou fréquence) d’une sous-population _B_ dans une population _A_ est le quotient de l’effectif de _B_ par l’effectif de _A_.</span>
    
    <span style="color: #FF0000">La proportion de _B_ dans _A_ est le nombre _p_ tel que :</span>
    
    <span style="color: #FF0000">_p_ = frac{nombre d'éléments de _B_}/{nombre d'éléments de _A_}</span>


!!! danger "Attention"

    Une proportion écrite sous forme décimale ou fractionnaire est toujours comprise en 0 et 1.


???+ question "Exercice :"

    <span style="color: #0000FF">Il y a 105 élèves de seconde du lycée Racan qui pratiquent un sport à l’extérieur du lycée.</span> 
    
    <span style="color: #0000FF">Le nombre d’élèves de seconde est de 175.</span> 
    
    <span style="color: #0000FF">Calculer la proportion d’élèves pratiquant un sport parmi les élèves de secondes.</span>

    ??? success "Solution"

        105 des 175 élèves de seconde du lycée Racan pratiquent un sport à l'extérieur du lycée d'où _p_ = 105/175 soit 3/5 soit encore 0,6.
        
        Donc la proportion d'élèves pratiquant un sport parmi les élèves de secondes est égale à 0,6.

## II. Pourcentage de pourcentage

!!! abstract "Propriété"

    <span style="color: #FF0000">_B_ et _C_ sont deux sous populations d’une population _A_ telles que _C_ est incluse dans _B_.</span> 
    
    <span style="color: #FF0000">Soit _p_ la proportion des éléments de _B_ dans _A_.</span>
    
    <span style="color: #FF0000">Soit _p’_ la proportion des éléments de _C_ dans _B_.</span>
    
    <span style="color: #FF0000">La proportion de _C_ dans _A_ est égale à _p_ × _p'_.</span>

???+ question "Exercice :"

    <span style="color: #0000FF">Dans la classe de seconde D du lycée Racan, il y a 45 % de garçons.</span> 
    
    <span style="color: #0000FF">De plus 15 % des garçons jouent dans un club de football.</span> 
    
    <span style="color: #0000FF">Calculer le pourcentage de garçons jouant au football en seconde D.</span>

    ??? success "Solution"

        La population _A_ est représentée par les élèves de la classe de seconde D.
        
        La population _B_ est représentée par les garçons de seconde D, c'est une sous population de _A_, d'où _p_ = 0,45.
        
        La population _C_ est représentée par les garçons de seconde D jouant dans un club de football, c'est une population incluse dans _B_, d'où _p'_ = 0,15.
        
        On a _p''_ = _p_ x _p'_ = 0,45 x 0,15 = 0,0675.
        
        Donc il y a 6,75 % de garçons jouant au football en seconde D.


## III. Augmentation et réduction de pourcentage
    
!!! abstract "Propriétés"

    <span style="color: #FF0000">**1.** Augmenter une quantité de _t_ %, c'est multiplier cette quantité par _C_ = 1 + _t_/100.</span>

    <span style="color: #FF0000">**2.** Diminuer une quantité de _t_ %, c'est multiplier cette quantité par _C_ = 1 - _t_/100.</span>

    <span style="color: #FF0000">_C_ est appelé le coefficient multiplicateur.</span>

???+ question "Exercices :"

    <span style="color: #0000FF">**1.**  Un article coûte 25 € et il subit une augmentation de 15 %. Quel est son nouveau prix ?</span>
    
    ??? success "Solution"

        Augmenter un nombre de 15% revient à le multiplier par 1 + 15/100 soit 1,15. D’où 25 × 1,15 = 28,75. 
        
        Donc le nouveau prix est 28,75€.
    
    <span style="color: #0000FF">**2.**  Un article coûte 25 € et il subit une réduction de 15 %. Quel est son nouveau prix ?</span>
    
    ??? success "Solution"
    
        Diminuer un nombre de 15% revient à le multiplier par 1 - 15/100 soit 0,85. D’où 25 × 0,85 = 21,25.
        
        Donc le nouveau prix est 21,25€.